#include <vector>
#include <string>
#include <iostream>
#include "MultiDimSignals.h"

MultiDimSignals::MultiDimSignals(Signal signal, int dimension)
{
    int vsize = signal.dimensionRanges.size(); // size of vector of dimensionRanges.

    int tsize = 1; // tsize will store total number of data types which we need to allocate.
    for (int i = 0; i < vsize - 1; i++)
    {
        tsize *= (signal.dimensionRanges[i].msb - signal.dimensionRanges[i].lsb + 1);
    }

    if (signal.dimensionRanges[vsize - 1].msb < 16 && signal.dimensionRanges[vsize - 1].lsb >= 0)
    {
        shortarray = (short *)malloc(tsize * sizeof(short)); // assigning the memory of total size required.
        std::cout << "\n"
                  << "We have allocated : " << (tsize * sizeof(short)) << " bytes of memory. "
                  << "\n"
                  << "\n";
    }
    else if (signal.dimensionRanges[vsize - 1].msb < 32 && signal.dimensionRanges[vsize - 1].lsb >= 0)
    {
        intarray = (int *)malloc(tsize * sizeof(int)); // assigning the memory of total size required.
        std::cout << "\n"
                  << "We have allocated : " << (tsize * sizeof(int)) << " bytes of memory in memory of CPU. "
                  << "\n"
                  << "\n";
    }
    else if (signal.dimensionRanges[vsize - 1].msb < 64 && signal.dimensionRanges[vsize - 1].lsb >= 0)
    {
        doublearray = (double *)malloc(tsize * sizeof(double)); // assigning the memory of total size required.
        std::cout << "\n"
                  << "We have allocated : " << (tsize * sizeof(double)) << " bytes of memory. "
                  << "\n"
                  << "\n";
    }
    else if (signal.dimensionRanges[vsize - 1].msb < 128 && signal.dimensionRanges[vsize - 1].lsb >= 0)
    {
        longdoublearray = (long double *)malloc(tsize * sizeof(long double)); // assigning the memory of total size required.
        std::cout << "\n"
                  << "We have allocated : " << (tsize * sizeof(long double)) << " bytes of memory. "
                  << "\n"
                  << "\n";
    }
    else
    {
        std::cerr << "ERROR : Memory of more than 128 bits can not be assigned ";
    }
}

void MultiDimSignals::setvectorofvalues(Signal signal, std::vector<int> startindex, std::vector<int> endindex, std::vector<int> values)
{
    int totalnumberofindexes = 0;
    int vsize = signal.dimensionRanges.size();
    for (int i = 0; i < startindex.size(); i++)
    {
        int temp = endindex[i] - startindex[i];
        for (int j = i + 1; j < vsize - 1; j++)
        {
            temp *= (signal.dimensionRanges[j].msb - signal.dimensionRanges[j].lsb + 1);
        }
        totalnumberofindexes += temp;
    }
    totalnumberofindexes++;

    int toadd = 0;
    for (int i = 0; i < vsize - 1; i++)
    {
        int temp = startindex[i];
        for (int j = i + 1; j < vsize; j++)
        {
            temp *= (signal.dimensionRanges[j].msb - signal.dimensionRanges[j].lsb + 1);
        }
        toadd += temp;
    }

    if (signal.dimensionRanges[vsize - 1].msb < 16 && signal.dimensionRanges[vsize - 1].lsb >= 0)
    {
        std::cout << "\n"
                  << "Data Type used for setting the values is => short"
                  << "\n";
        short *fillvalue = (shortarray + toadd);
        for (int i = 0; i < totalnumberofindexes; i++)
        {
            std::cout << values[i] << " : this value is going to set in this adrress " << &(*(fillvalue + i)) << "\n";
            *(fillvalue + i) = values[i];
        }
        std::cout << "\n";
    }
    else if (signal.dimensionRanges[vsize - 1].msb < 32 && signal.dimensionRanges[vsize - 1].lsb >= 0)
    {
        std::cout << "Data Type used for setting the values is => int"
                  << "\n"
                  << "\n";
        int *fillvalue = (intarray + toadd);
        for (int i = 0; i < totalnumberofindexes; i++)
        {
            std::cout << values[i] << " : this value is going to set in this adrress " << &(*(fillvalue + i)) << "\n";
            *(fillvalue + i) = values[i];
        }
        std::cout << "\n";
    }
    else if (signal.dimensionRanges[vsize - 1].msb < 64 && signal.dimensionRanges[vsize - 1].lsb >= 0)
    {
        std::cout << "Data Type used for setting the values is => double"
                  << "\n"
                  << "\n";
        double *fillvalue = (doublearray + toadd);
        for (int i = 0; i < totalnumberofindexes; i++)
        {
            std::cout << values[i] << " : this value is going to set in this adrress " << &(*(fillvalue + i)) << "\n";
            *(fillvalue + i) = values[i];
        }
        std::cout << "\n";
    }
    else if (signal.dimensionRanges[vsize - 1].msb < 128 && signal.dimensionRanges[vsize - 1].lsb >= 0)
    {
        std::cout << "Data Type used for setting the values is => long double"
                  << "\n"
                  << "\n";
        long double *fillvalue = (longdoublearray + toadd);
        for (int i = 0; i < totalnumberofindexes; i++)
        {
            std::cout << values[i] << " : this value is going to set in this adrress " << &(*(fillvalue + i)) << "\n";
            *(fillvalue + i) = values[i];
        }
        std::cout << "\n";
    }
    else
    {
        std::cerr << "ERROR : Memory of more than 128 bits can not be set in memory location "
                  << "\n";
    }
}

std::vector<int> MultiDimSignals::getvectorofvalues(Signal signal, std::vector<int> startindex, std::vector<int> endindex, std::vector<int> values)
{
    std::vector<int> returnedvalues;
    int totalnumberofindexes = 0;
    int vsize = signal.dimensionRanges.size();
    for (int i = 0; i < startindex.size(); i++)
    {
        int temp = endindex[i] - startindex[i];
        for (int j = i + 1; j < vsize - 1; j++)
        {
            temp *= (signal.dimensionRanges[j].msb - signal.dimensionRanges[j].lsb + 1);
        }
        totalnumberofindexes += temp;
    }
    totalnumberofindexes++;

    int toadd = 0;
    for (int i = 0; i < vsize - 1; i++)
    {
        int temp = startindex[i];
        for (int j = i + 1; j < vsize; j++)
        {
            temp *= (signal.dimensionRanges[j].msb - signal.dimensionRanges[j].lsb + 1);
        }
        toadd += temp;
    }

    if (signal.dimensionRanges[vsize - 1].msb < 16 && signal.dimensionRanges[vsize - 1].lsb >= 0)
    {
        short *fillvalue = (shortarray + toadd);
        for (int i = 0; i < totalnumberofindexes; i++)
        {
            returnedvalues.push_back(*fillvalue);
            if (i != (totalnumberofindexes - 1))
                fillvalue++;
        }
    }
    else if (signal.dimensionRanges[vsize - 1].msb < 32 && signal.dimensionRanges[vsize - 1].lsb >= 0)
    {
        int *fillvalue = (intarray + toadd);
        for (int i = 0; i < totalnumberofindexes; i++)
        {
            returnedvalues.push_back(*fillvalue);
            if (i != (totalnumberofindexes - 1))
                fillvalue++;
        }
    }
    else if (signal.dimensionRanges[vsize - 1].msb < 64 && signal.dimensionRanges[vsize - 1].lsb >= 0)
    {
        double *fillvalue = (doublearray + toadd);
        for (int i = 0; i < totalnumberofindexes; i++)
        {
            returnedvalues.push_back(*fillvalue);
            if (i != (totalnumberofindexes - 1))
                fillvalue++;
        }
    }
    else if (signal.dimensionRanges[vsize - 1].msb < 128 && signal.dimensionRanges[vsize - 1].lsb >= 0)
    {
        long double *fillvalue = (longdoublearray + toadd);
        for (int i = 0; i < totalnumberofindexes; i++)
        {
            returnedvalues.push_back(*fillvalue);
            if (i != (totalnumberofindexes - 1))
                fillvalue++;
        }
    }
    else
    {
        std::cerr << "ERROR : Memory of more than 128 bits can not be accessed "
                  << "\n";
    }
    return returnedvalues;
}
