#include <vector>
#include <string>
#include <iterator>
#include <iostream>

#include "MultiDimSignals.h"

int main()
{
    std::cout<<"I am using git-_- again";
    // A sample structure of signal. i.e.   sigA[2:1][3:0][4:2][5:3][6:3][7:0]
    Signal check;
    check.signalname = "sigA";
    check.dimensionRanges = {{0, 0}, {1, 0}, {2, 0}, {31, 0}};
    int sizeOfDimensionRangeVector = check.dimensionRanges.size();

    // we need to set vector of values in contiguos memory  from a particular starting index to ending index
    std::vector<int> startindex = {0, 0, 0};
    std::vector<int> endindex = {0, 1, 1};
    std::vector<int> values = {5, 10, 15, 32888, 25};

    std::cout << "\n"
              << "constructor called...";
    // An object created and intialised of MultiDimSignals class.
    MultiDimSignals tempsignal(check, 4);

    std::cout << "Total number of values we need to set is equal to : " << values.size()
              << "\n"
              << "\n";

    std::cout << "The values, We need to set at given indexes are : "
              << "\n";
    for (int i = 0; i < values.size(); i++)
    {
        std::cout << values[i] << " ";
    }
    std::cout << "\n"
              << "\n";

    // setting the values from a particular starting index to ending index.
    tempsignal.setvectorofvalues(check, startindex, endindex, values);
    std::cout << "All values are set on contiguos memory locations."
              << "\n"
              << "\n"
              << "\n";

    // finding the values at given index.
    std::vector<int> res;
    res = tempsignal.getvectorofvalues(check, startindex, endindex, values);
    std::cout << "Accessing all the needed values : "
              << "\n";
    for (int i = 0; i < res.size(); i++)
    {
        std::cout << res[i] << "\n";
    }
    return 0;
}