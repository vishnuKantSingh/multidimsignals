#include <vector>
#include <string>
#include <iterator>

#ifndef MULTIDIMSIGNALS_H
#define MULTIDIMSIGNALS_H

struct range
{
    int msb; // for dimension range exp [3:0] then lsb=0 and msb=3 .
    int lsb;
};

struct Signal
{
    std::string signalname;             // name of multi dim array.
    std::vector<range> dimensionRanges; // all dimension ranges will be in this form  i.e. [3:0] => {3,0}.
};

class MultiDimSignals
{
public:
    short *shortarray = NULL;            // size => 2 bytes or 16 bits (useful only if values lie in this range).
    int *intarray = NULL;                // size => 4 bytes or 32 bits (useful only if values lie in this range).
    double *doublearray = NULL;          // size => 8 bytes or 64 bits (useful only if values lie in this range).
    long double *longdoublearray = NULL; // size => 16 bytes or 128 bits (useful only if values lie in this range).

public:
    MultiDimSignals(){
        // nothing here for default constructor.
    };
    MultiDimSignals(Signal sig, int dimension); // parameterized constructor .

    // It will set vector of values from some starting index to ending index.
    void setvectorofvalues(Signal sig, std::vector<int> startindex, std::vector<int> endindex, std::vector<int> values);

    // It will return vector of values from some starting index to ending index.
    std::vector<int> getvectorofvalues(Signal sig, std::vector<int> startindex, std::vector<int> endindex, std::vector<int> values);

    // freeing the allocated memory.
    ~MultiDimSignals()
    {
        free(shortarray);
        free(intarray);
        free(doublearray);
        free(longdoublearray);
    }
};

#endif